//Load express
var express = require("express");
var path = require("path");

//Create an instance of the express application
var app = express();
const PORT = 3030;
const CLIENT_FOLDER = path.join(__dirname + '/../client');  // CLIENT FOLDER is the public directory

app.use(express.static(CLIENT_FOLDER));

// //Start the web server on port 3030
// app.listen(PORT, function() {
//     console.info("Web Server started on port 3030");
//     console.info(__dirname);
// });

app.listen(PORT, function () {
    console.log("%s \nApplication started at %s:%d", new Date(), __dirname, PORT);
});